import {
  Given,
  When,
  And,
  Then,
  Before,
  After,
} from "@badeball/cypress-cucumber-preprocessor";
const homePage = require("../../pages/HomePage");

Given("the user is on the home page", () => {
  cy.visit("https://www.iislafe.es/es/");
  cy.screenshot()
});
When("the user searchs for {string}", (text) => {
  homePage.clickOnloupe();
  homePage.typeOnSearchBox(text);
  cy.screenshot()
  //cy.get('.post').screenshot()
  homePage.typeEnterInsideSearchBox();
});
Then("the user should see the search url of the new page", () => {
  cy.url().should("contains", "/buscador/?page=1&keywords=surpass");
});

after('Getting device and browser info', () => {
  cy.saveFeatureMetadata();
});





