import {
  Given,
  When,
  And,
  Then,
  Before
} from "@badeball/cypress-cucumber-preprocessor";
const loginPage = require("../../pages/LoginPage");
const credentialsPath = "../credentials_and_other/credentials.json";
const sidebar = require("../../pages/Sidebar");
const topbar = require("../../pages/Topbar");

  
function beforeClass(){

   cy.visit("/");
    cy.readFile(credentialsPath).then((credentials) => {
      const enteredUsername = credentials.username;
      loginPage.typeUsername(enteredUsername);
      const password = credentials.password;
      loginPage.typePassword(password, {log: false});
      loginPage.clickLogin();
  
    });
}

Given("The user is on the home page", () => {
    beforeClass();
      cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/");
      cy.screenshot();
});
Given("The user is on the patients list page", () => {
    beforeClass();
   cy.visit("https://psp-sp.prep.sanit.cineca.it/study/passport/index.php?list=patients_list.xml");
   cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?list=patients_list.xml");
   cy.screenshot();
});


Given("The user is on the new registration page", () => {
    beforeClass();
   cy.visit("https://psp-sp.prep.sanit.cineca.it/study/passport/index.php?ESAM=0&VISITNUM=0");
   cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?ESAM=0&VISITNUM=0");
   cy.screenshot();
});

Given("The user is on the search page", () => {
    beforeClass();
    cy.visit("https://psp-sp.prep.sanit.cineca.it/study/passport/index.php?SEARCH=1&FORM=1&PAGE=1");
    cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?SEARCH=1&FORM=1&PAGE=1");
    cy.screenshot();
});

Given("The user is on the import data page", () => {
    beforeClass();
    cy.visit("https://psp-sp.prep.sanit.cineca.it/study/passport/index.php?FHIR_CONFIG");
    cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?FHIR_CONFIG");
    cy.screenshot();
});

Given("The user is on the view data page", () => {
    beforeClass();
    cy.visit("https://psp-sp.prep.sanit.cineca.it/study/passport/index.php?list=patients_fhir_list.xml");
    cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?list=patients_fhir_list.xml");
    cy.screenshot();
});


Given("The user is not on the home page", () => {
    beforeClass();
    cy.visit("https://psp-sp.prep.sanit.cineca.it/study/passport/index.php?list=patients_fhir_list.xml");
    cy.url().should("contains", "index.php");
});


When("the user click on SurPass button on the sidebar", () => {
    sidebar.clickOnHomePage();
    sidebar.elements.homPagebutton().screenshot();
});

When("the user click on data Data Entry button on sidebar", () => {
    sidebar.clickOnDropdownDataEntry();
    sidebar.elements.dropdownDataEntry().screenshot();
});
When("the user click on data integration dropdown button on sidebar", () => {
    sidebar.clickOnDropdownIntegrationData();
    sidebar.elements.dropdownIntegrationData().screenshot();
});

When("the user click on Survivorship Passport o the blue top bar", () => {
    topbar.clickOnNavbarHomeLink();
    topbar.elements.navbarHomeLink().screenshot();
});

And("then on import data", () => {
    sidebar.clickOnimportData();
    sidebar.elements.importDatabutton().screenshot();
});

And("then on view data button", () => {
    sidebar.clickOnviewData();
    sidebar.elements.viewDatabutton().screenshot();
});


And("after that on the Patients List", () => {
    sidebar.clickOnPatientList();
    sidebar.elements.patientlist().screenshot();
});
And("then on search button", () => {
    sidebar.clickOnSearch();
    sidebar.elements.search().screenshot();
});
And("then on new registration", () => {
    sidebar.clickOnRegistryButton();
    sidebar.elements.registrybutton().screenshot();
});

Then("the user can see the patients list page", () => {
    cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?list=patients_list.xml");
    cy.screenshot();
});

Then("the user can see the new registration page", () => {
    cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?ESAM=0&VISITNUM=0");
    cy.screenshot();
});

Then("the user can see the search page", () => {
    cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?SEARCH=1&FORM=1&PAGE=1");
    cy.screenshot();
}); 

Then("the user can see the fhir data page", () => {
    cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/index.php?list=patients_fhir_list.xml");
    cy.screenshot();
});

Then("the user can see the import data page", () => {
    cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/?FHIR_CONFIG");
    cy.screenshot();
});

Then("the user can see the home page", () => {
    cy.url().should("eq", "https://psp-sp.prep.sanit.cineca.it/study/passport/index.php");
    cy.screenshot();
});
Then("the user can see some data on the patients list page", () => {
    cy.get("table").should("be.visible");
    cy.screenshot();
});

Then("the user can see some data on the fhir data list page", () => {
    cy.get("table").should("be.visible");
    cy.screenshot();
});
