import {
  Given,
  When,
  And,
  Then,
} from "@badeball/cypress-cucumber-preprocessor";
const loginPage = require("../../pages/LoginPage");

const credentialsPath = "../credentials_and_other/credentials.json";

Given("A user opens the Surpass Platform", () => {
  cy.visit("/");
});
When("A user enters the username {string}", (username) => {
  let enteredUsername = username;
  cy.log(enteredUsername);
  if(username === "admin") {
    cy.readFile(credentialsPath).then((credentials) => {
      enteredUsername = credentials.username;
      loginPage.typeUsername(enteredUsername);
      //cy.screenshot();
    });
  }else {
    loginPage.typeUsername(enteredUsername);
    cy.screenshot();
  }
});

And("A user enters the correct username {string}", (username) => {
  let enteredUsername = username;
  cy.log(enteredUsername);
  if(username === "admin") {
    // read username from external file using readFileSync
    cy.readFile(credentialsPath).then((credentials) => {
      enteredUsername = credentials.username;
      loginPage.typeUsername(enteredUsername);
      //cy.screenshot();
    });
  }else {
    loginPage.typeUsername(enteredUsername);
    cy.screenshot();
  }
});

When("A user provides incorrect credentials", (table) => {
  table.hashes().forEach((row) => {
    cy.log(row.username);
    cy.log(row.password);
    loginPage.typeUsername(row.username);
    loginPage.typePassword(row.password);
  });
});
And("A user enters the password {string}", (password) => {
  if(password === "admin") {
  cy.readFile(credentialsPath).then((credentials) => {
      password = credentials.password;
      loginPage.typePassword(password, {log: false});
      //loginPage.elements.loginBox().screenshot();
    });
  }else{
    loginPage.typePassword(password);
    cy.screenshot();
  }
});

And("A user enters the correct password {string}", (password) => {
  if(password === "admin") {
  cy.readFile(credentialsPath).then((credentials) => {
      password = credentials.password;
      loginPage.typePassword(password, {log: false});
      //loginPage.elements.loginBox().screenshot();
    });
  }else{
    loginPage.typePassword(password);
    cy.screenshot();
  }
});



And("A user clicks on the login button", () => {
  loginPage.clickLogin();
});
Then("the url will contain the basic url of the home page", () => {
  cy.url().should("contains", "psp-sp.prep.sanit.cineca.it/study/passport/");
  cy.screenshot();
});

Then("The error message {string} is displayed", (errorMessage) => {
  loginPage.elements.errorMessage().contains(errorMessage);
  loginPage.elements.loginBox().screenshot();
});

When("A user waits some {string} miliseconds without doing anything", (time) => {
  let waitTime  = parseInt(time);
  cy.wait(waitTime);
  loginPage.elements.loginBox().screenshot();
});






after('Getting device and browser info', () => {
  cy.saveFeatureMetadata();
});
