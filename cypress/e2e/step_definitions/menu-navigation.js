import {
  Given,
  When,
  And,
  Then,
  Before,
  After,
} from "@badeball/cypress-cucumber-preprocessor";
const menuPage = require("../../pages/MenuPage");

Given("I am on the home page", () => {
  cy.visit("https://www.iislafe.es/es/");
  cy.screenshot();
});
When("I click the menu link for the Investigacion", (text) => {
  menuPage.elements.investigacionLink().should('have.attr', 'href')
  menuPage.clickOnInvestigacionLink();
  menuPage.elements.investigacionLink().screenshot();
});
Then("I am taken to the Investigacion page", () => {
    menuPage.checkInstitucionalLink();
    cy.screenshot();
});
When("I click the menu link for the Sala de Prensa", (text) => {
  menuPage.elements.salaLink().should('have.attr', 'href')
  menuPage.elements.salaLink().screenshot();
  menuPage.clickOnSalaLink();
});
Then("I am taken to the Sala de Prensa page", () => {
    cy.log('This is a mistaken link, its an example of error test.')
    menuPage.checkSalaLink();
    cy.screenshot();
});

after('Getting device and browser info', () => {
  cy.saveFeatureMetadata();
});





