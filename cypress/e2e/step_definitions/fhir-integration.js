import {
  Given,
  When,
  And,
  Then,
  Before
} from "@badeball/cypress-cucumber-preprocessor";
import ViewDataPage from "../../pages/ViewDataPage";
const loginPage = require("../../pages/LoginPage");
const credentialsPath = "../credentials_and_other/credentials.json";
const sidebar = require("../../pages/Sidebar");
const topbar = require("../../pages/Topbar");
const importPage = require("../../pages/ImportPage");
const viewDataPage = require("../../pages/ViewDataPage");



 When("A user type the username {string}", (username) => {
  let enteredUsername = username;
  cy.log(enteredUsername);
  if(username === "admin") {
    cy.readFile(credentialsPath).then((credentials) => {
      enteredUsername = credentials.apiuser;
      importPage.typeUsername(enteredUsername);
      //cy.screenshot();
    });
  }else {
    importPage.typeUsername(enteredUsername);
    cy.screenshot();
  }
});

And("A user type the password {string}", (password) => {
  if(password === "admin") {
  cy.readFile(credentialsPath).then((credentials) => {
      password = credentials.apipass;
      importPage.typePassword(password, {log: false});
      //loginPage.elements.loginBox().screenshot();
    });
  }else{
    importPage.typePassword(password);
    cy.screenshot();
  }
});

Then("push the import button", () => {
  importPage.clickLogin();
  importPage.clickOnConfirm();
});


When("the user click on the import button for patients", (table) => {
  table.hashes().forEach((row) => {
    cy.log("patient "+row.patient);
    cy.log("position "+row.position);
    viewDataPage.clickOnPatientDataImport(row.position);
  });
});

Then("the user can see the patient imported tag from fhir server", (table) => {
  table.hashes().forEach((row) => {
    viewDataPage.getImportedTag(row.position);
  });
});

