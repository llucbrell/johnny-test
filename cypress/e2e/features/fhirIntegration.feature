@Fhir-integration
Feature: Platform Fhir data integration
Feature for data integration from FHIR server with Cineca Platform

Background: The user is logged in the Surpass Platform
   
   @Fhir-integration
    Scenario: The user can import data from FHIR server
        Given The user is on the import data page
        When A user type the username "admin"
        And A user type the password "admin"
        Then push the import button
    @Fhir-integration
    Scenario: The user can import a patient from fhir server
        Given The user is on the view data page
        When the user click on the import button for patients 
        |patient| position|
        |1      | 1       |
        |2      | 2       |
        |3      | 3       |
        |4      | 4       |
        |5      | 5       |
        |6      | 6       |
        |7      | 7       |
        |8      | 8       |
        |9      | 9       |
        Then the user can see the patient imported tag from fhir server
        |patient| position|
        |1      | 1       |
        |2      | 2       |
        |3      | 3       |
        |4      | 4       |
        |5      | 5       |
        |6      | 6       |
        |7      | 7       |
        |8      | 8       |
        |9      | 9       |
        
    @Fhir-integration @Fhir-model @Subject-of-care
    Scenario: The user can check if the patient fhir model is correctly imported 
        Given The user is on the patients list page
        When the user click on patient 1
        Then the patient demographic model is loaded correctly
    @Fhir-integration @Fhir-model @Subject-of-care @Diagnosis @Transaction
    Scenario: The user can check if the patient fhir model is correctly imported 
        Given The user is on the patients list page
        When the user click on patient 2
        Then the patient diagnosis model is loaded correctly
    @Fhir-integration @Fhir-model @Subject-of-care @Diagnosis @Chemotherapy @Transaction
    Scenario: The user can check if the patient fhir model is correctly imported 
        Given The user is on the patients list page
        When the user click on patient 3
        Then the patient chemotherapy model is loaded correctly
    @Fhir-integration @Fhir-model @Subject-of-care @Diagnosis @Chemotherapy @Major-surgery @Transaction
    Scenario: The user can check if the patient fhir model is correctly imported 
        Given The user is on the patients list page
        When the user click on patient 4
        Then the patient major surgery model is loaded correctly
    @Fhir-integration @Fhir-model @Subject-of-care @Diagnosis @Chemotherapy @Major-surgery @Stem-cell @Transaction
    Scenario: The user can check if the patient fhir model is correctly imported 
        Given The user is on the patients list page
        When the user click on patient 5
        Then the patient stem cell transplantation model is loaded correctly
 
