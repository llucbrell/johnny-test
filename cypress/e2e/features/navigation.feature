@Navigation
Feature: Platform Basic Navigation
Feature Login that checks if the user can navigate through the platform without errors

Background: The user is logged in the Surpass Platform
   
   @Only-browser
    Scenario: The user can navigate to the list page
        Given The user is on the home page
        When the user click on data Data Entry button on sidebar
        And after that on the Patients List
        Then the user can see the patients list page
    @Only-browser
    Scenario: The user can navigate to the new registration page
        Given The user is on the home page
        When the user click on data Data Entry button on sidebar
        And then on new registration
        Then the user can see the new registration page
    @Only-browser
    Scenario: The user can navigate to the data entry page 
        Given The user is on the home page
        When the user click on data Data Entry button on sidebar
        And then on search button
        Then the user can see the search page
    @Only-browser
    Scenario: The user can navigate to the import data page
        Given The user is on the home page
        When the user click on data integration dropdown button on sidebar
        And then on import data
        Then the user can see the import data page
    @Only-browser
    Scenario: The user can navigate to the view data page 
        Given The user is on the home page
        When the user click on data integration dropdown button on sidebar
        And then on view data button
        Then the user can see the fhir data page
    @Only-browser
    Scenario: The user can navigate to the home page
        Given The user is on the patients list page
        When the user click on SurPass button on the sidebar
        Then the user can see the home page
    @Only-browser
    Scenario: The user can navigate to the home page
        Given The user is on the new registration page
        When the user click on SurPass button on the sidebar
        Then the user can see the home page
    @Only-browser
    Scenario: The user can navigate to the home page
        Given The user is on the search page
        When the user click on SurPass button on the sidebar
        Then the user can see the home page
    @Only-browser
    Scenario: The user can navigate to the home page
        Given The user is on the import data page
        When the user click on SurPass button on the sidebar
        Then the user can see the home page
    @Only-browser
    Scenario: The user can navigate to the home page
        Given The user is on the view data page
        When the user click on SurPass button on the sidebar
        Then the user can see the home page
    @Only-browser
    Scenario: The user can navigate to the home page
        Given The user is not on the home page 
        When the user click on Survivorship Passport o the blue top bar
        Then the user can see the home page
    @Api-dependency
    Scenario: The user can see data in the patients list page 
        Given The user is on the home page
        When the user click on data Data Entry button on sidebar
        And after that on the Patients List
        Then the user can see some data on the patients list page
    @Api-dependency
    Scenario: The user can see data in the fhir data list page 
        Given The user is on the home page
        When the user click on data integration dropdown button on sidebar
        And then on view data button
        Then the user can see some data on the fhir data list page
