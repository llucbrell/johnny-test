@Access @Security @Login
Feature: Login

    Feature Login page will work depending on the user credentials.
    @Only-browser
    Scenario: Blocked Login
        Given A user opens the Surpass Platform
        When A user enters the username "locked_out_user"
        And A user enters the password "secret_sauce"
        And A user clicks on the login button
        Then The error message "Invalid username or password" is displayed
    @Only-browser
    Scenario: Incorrect Username Login
        Given A user opens the Surpass Platform
        When A user provides incorrect credentials
            | username | password     |
            | testName | secret_sauce |
            | user_lock | secret_secret |
        And A user clicks on the login button
        Then The error message "Invalid username or password" is displayed
    @Only-browser
    Scenario: Incorrect Password Login
        Given A user opens the Surpass Platform
        When A user provides incorrect credentials
            | username      | password     |
            | standard_user | testPassword |
        And A user clicks on the login button
        Then The error message "Invalid username or password" is displayed
    @Only-browser
    Scenario: Success Login
        Given A user opens the Surpass Platform
        When A user enters the username "admin"
        And A user enters the password "admin"
        And A user clicks on the login button
        Then the url will contain the basic url of the home page
    @skip @Only-browser
    Scenario: No loggin after some innactivity time
        Given A user opens the Surpass Platform
        When A user waits some "000" miliseconds without doing anything
        And A user enters the correct username "admin"
        And A user enters the correct password "admin"
        And A user clicks on the login button
        Then The error message "Time Out" is displayed
        