// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('getFeatureMetadata', () => {
    return getMetadata();
});
Cypress.Commands.add('saveFeatureMetadata', () => {
    // check if metadata file exist
 // if (fs.existsSync('./metadata.json')) {
        // if exist, read the file
    cy.getFeatureMetadata().then((metadata) => {
        cy.fixture('../../misc/metadata/features-metadata.json').then((data) => {
            data.push(metadata);
            cy.writeFile('./misc/metadata/features-metadata.json', JSON.stringify(data, undefined, 4));
        });
    });
    /*
  } else {
        // if not exist, create the file and save the metadata
    cy.getFeatureMetadata().then((metadata) => {
        cy.writeFile('cypress/fixtures/metadataaaa.json', JSON.stringify([metadata], undefined, 4));
    });
  }
*/
});

function getMetadata() {
return metadata = {
        name: Cypress.currentTest.titlePath[0],
        metadata: {
            browser:{
                name: Cypress.browser.name,
                version: Cypress.browser.version
            },
            device: "Cypress "+Cypress.testingType,
            platform: {
                name: Cypress.platform,
                version: Cypress.arch,
                isHeaded: Cypress.isHeaded,
                channel: Cypress.channel,
                },
            },
            testingFramework: {
                name: "Cypress",
                version: Cypress.version,
                testingType: Cypress.testingType,
                },
    }
}




