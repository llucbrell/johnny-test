class topbar {
  elements = {
    navbarHomeLink: () => cy.get('.navbar-brand > small'),
  };

  clickOnNavbarHomeLink() {
    this.elements.navbarHomeLink().click();
  }
 
}
module.exports = new topbar;