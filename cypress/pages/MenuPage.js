class MenuPage {
  elements = {
    investigacionLink: () => cy.get(':nth-child(2) > [aria-haspopup="true"]'),
    salaLink: () => cy.get(':nth-child(6) > [aria-haspopup="true"]'),
  };

  clickOnInvestigacionLink(url) {
    this.elements.investigacionLink().click();
}
  checkInstitucionalLink() {
    cy.url().should("contains", "investigacion/lineas-de-investigacion/");
}



  clickOnSalaLink(url) {
    this.elements.investigacionLink().click();
  }
  checkSalaLink() {
    cy.url().should("contains", "actualidad");
}


}

module.exports = new MenuPage();
