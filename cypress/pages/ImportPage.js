class ImportPage {
  elements = {
    usernameInput: () =>cy.get('[name="username"]'),
    passwordInput: () => cy.get('[name="password"]'),
    loginBtn: () =>cy.get('#retrieve'),
    confirmDialog: () => cy.get('[data-bb-handler="confirm"]'),
  };

  typeUsername(username) {
    this.elements.usernameInput().clear().type(username);
  }

  typePassword(password, options) {
    if(options){
      this.elements.passwordInput().type(password, options);
    }else{
      this.elements.passwordInput().type(password);
    }
  }

  clickLogin() {
    this.elements.loginBtn().click();
  }


  clickOnConfirm(){
    this.elements.confirmDialog().click();
  }


}

module.exports = new ImportPage();
