class HomePage {
  elements = {
    searchboxInput: () => cy.get("#s1"),
    openSearchBox: () => cy.get('label > img'),
  };

  clickOnloupe() {
    this.elements.openSearchBox().click();
  }

  typeOnSearchBox(text) {
    this.elements.searchboxInput().type(text);
  }
  typeEnterInsideSearchBox() {
    this.elements.searchboxInput().type("{enter}");
  }

}

module.exports = new HomePage();
