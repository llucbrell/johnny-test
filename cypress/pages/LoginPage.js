class homeSaucePage {
  elements = {
    usernameInput: () => cy.get("#username"),
    passwordInput: () => cy.get("#password"),
    loginBtn: () => cy.get("#kc-login"),
    loginBox: () => cy.get(".card-pf"),
    errorMessage: () => cy.get('#input-error'),
  };

  typeUsername(username) {
    this.elements.usernameInput().type(username);
  }

  typePassword(password, options) {
    if(options){
      this.elements.passwordInput().type(password, options);
    }else{
      this.elements.passwordInput().type(password);
    }
  }

  clickLogin() {
    this.elements.loginBtn().click();
  }
}

module.exports = new homeSaucePage();
