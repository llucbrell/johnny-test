class sidebar {
  elements = {
    dropdownDataEntry: () => cy.get(':nth-child(2) > .dropdown-toggle > .menu-text'),
    registrybutton : () => cy.get('.open > .submenu > :nth-child(1) > a > .menu-text'),
    search : () => cy.get('.open > .submenu > :nth-child(2) > a > .menu-text'),
    patientlist : () => cy.get('.open > .submenu > :nth-child(3) > a > .menu-text'),
    dropdownIntegrationData: () => cy.get(':nth-child(3) > .dropdown-toggle > .menu-text'),
    importDatabutton: () => cy.get('.open > .submenu > :nth-child(1) > a > .menu-text'),
    viewDatabutton: () => cy.get('.open > .submenu > :nth-child(2) > a > .menu-text'),
    homPagebutton: () => cy.get('.nav > :nth-child(1) > a > .menu-text'),
  };

  clickOnHomePage() {
    this.elements.homPagebutton().click();
  }
  clickOnimportData() {
    this.elements.importDatabutton().click();
  }
  clickOnviewData() {
    this.elements.viewDatabutton().click();
    }
  clickOnSidebarItem(item) {
    cy.get(`[data-cy=${item}]`).click();
  }
  clickOnDropdownIntegrationData() {
    this.elements.dropdownIntegrationData().click();
  }
  clickOnDropdownDataEntry() {
    this.elements.dropdownDataEntry().click();
  }
  clickOnRegistryButton() {
    this.elements.registrybutton().click();
  }
  clickOnSearch() {
    this.elements.search().click();
  }
  clickOnPatientList() {
    this.elements.patientlist().click();
  }
} 


module.exports = new sidebar;