class ViewDataPage {
  elements = {
    importBtn1: () =>cy.get(':nth-child(1) > :nth-child(7) > a > .fa'),
    importBtn2: () =>cy.get(':nth-child(2) > :nth-child(7) > a > .fa'),
    importBtn3: () =>cy.get(':nth-child(3) > :nth-child(7) > a > .fa'),
    importBtn4: () =>cy.get(':nth-child(4) > :nth-child(7) > a > .fa'),
    importBtn5: () =>cy.get(':nth-child(5) > :nth-child(7) > a > .fa'),
    importBtn6: () =>cy.get(':nth-child(6) > :nth-child(7) > a > .fa'),
    importBtn7: () =>cy.get(':nth-child(7) > :nth-child(7) > a > .fa'),
    importBtn8: () =>cy.get(':nth-child(8) > :nth-child(7) > a > .fa'),
    importBtn9: () =>cy.get(':nth-child(9) > :nth-child(7) > a > .fa'),
    confirmDialog: () => cy.get('[data-bb-handler="confirm"]'),
    importStatus1: () => cy.get('#lista_table_body > :nth-child(1) > :nth-child(6)'),
    importStatus2: () => cy.get('#lista_table_body > :nth-child(2) > :nth-child(6)'),
    importStatus3: () => cy.get('#lista_table_body > :nth-child(3) > :nth-child(6)'),
    importStatus4: () => cy.get('#lista_table_body > :nth-child(4) > :nth-child(6)'),
    importStatus5: () => cy.get('#lista_table_body > :nth-child(5) > :nth-child(6)'),
    importStatus6: () => cy.get('#lista_table_body > :nth-child(6) > :nth-child(6)'),
    importStatus7: () => cy.get('#lista_table_body > :nth-child(7) > :nth-child(6)'),
    importStatus8: () => cy.get('#lista_table_body > :nth-child(8) > :nth-child(6)'),
    importStatus9: () => cy.get('#lista_table_body > :nth-child(9) > :nth-child(6)'),
  };

   clickOnPatientDataImport(number) {
    switch (number) {
        case "1":
            this.elements.importBtn1().click();
            break;
        case "2":
            this.elements.importBtn2().click();
            break;
        case "3":
            this.elements.importBtn3().click();
            break;
        case "4":
            this.elements.importBtn4().click();
            break;
        case "5":
            this.elements.importBtn5().click();
            break;
        case "6":
            this.elements.importBtn6().click();
            break;
        case "7":
            this.elements.importBtn7().click();
            break;
        case "8":
            this.elements.importBtn8().click();
            break;
        case "9":
            this.elements.importBtn9().click();
            break;
        default:
            break;
    }
  }

   getImportedTag(number) {
    switch (number) {
        case "1":
            this.elements.importStatus1().should('contains.text', 'Imported');
            break;
        case "2":
            this.elements.importStatus2().should('contains.text', 'Imported');
            break;
        case "3":
            this.elements.importStatus3().should('contains.text', 'Imported');
            break;
        case "4":
            this.elements.importStatus4().should('contains.text', 'Imported');
            break;
        case "5":
            this.elements.importStatus5().should('contains.text', 'Imported');
            break;
        case "6":
            this.elements.importStatus6().should('contains.text', 'Imported');
            break;
        case "7":
            this.elements.importStatus7().should('contains.text', 'Imported');
            break;
        case "8":
            this.elements.importStatus8().should('contains.text', 'Imported');
            break;
        case "9":
            this.elements.importStatus9().should('contains.text', 'Imported');
            break;
        default:
            break;
    }
  }




}

module.exports = new ViewDataPage();

