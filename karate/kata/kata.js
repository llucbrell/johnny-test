const Handlebars = require("handlebars");
const fs = require("fs");
const csv = require('csv-parser');
const chalk = require('chalk');
const uuid = require('uuid');

class kata {
    constructor(options) {
        this.name = options.name;
        this.id = options.id;
        this.budo = {};
        this.budo.filePath = options.budo.filePath;
        this.output = {};
        this.tatami = {};
        this.tatami.input = "";
        this.tatami.filePath = options.tatami.filePath;
        this.kata = {};
        this.kata.output = options.kata.output;
        this.kata.filePath = options.kata.filePath;
        this.kata.fileType = options.kata.fileType;
        this.setHelpers();

   }

    setTatami(){
        //this.tatami = "Name: {{name}}";
        this.tatami.input = fs.readFileSync(this.tatami.filePath, 'utf8');
    }

    setBudo(){
        // read a json file and set the budo object
        this.budo = JSON.parse(fs.readFileSync(this.budo.filePath, 'utf8'));
    }
    kiai(){
        this.prepare(this.runHandlebars);
    }

        prepare(callback){
                this.setTatami();
                this.setBudo();
            if(!this.kata.fileType || this.kata.fileType === 'json'){
                this.budo.kata = JSON.parse(fs.readFileSync(this.kata.filePath, 'utf8'));
                callback(this);
            }
            else if(this.kata.fileType === 'csv'){

                const results = [];
                fs.createReadStream(this.kata.filePath)
                .pipe(csv())
                .on('data', (data) => results.push(data))
                .on('end', () => {
                    this.budo.kata = results;
                    callback(this);
                });

            }
 

        }
        runHandlebars(that){
                that.oosh();
                that.excuteKata();
        }

    oosh(){
        const tempTemplate = Handlebars.compile(this.tatami.input);
        this.output.txt = tempTemplate(this.budo);
    }
    excuteKata(){
        if( !this.kata.output || this.kata.output.outputSystem === 'console'){
            console.log("---------------------- kata " + this.id + " ----------------------\n\n");
            console.log(chalk.gray(this.output.txt));
            console.log("\n---------------------------------------------------------------------------------------\n");
        }
        else if(this.kata.output.outputSystem === 'file'){
            fs.writeFileSync(this.kata.output.outputFile, this.output.txt);
        }
    }
    setHelpers(){

        Handlebars.registerHelper({
            eq: (v1, v2) => v1 === v2,
            ne: (v1, v2) => v1 !== v2,
            lt: (v1, v2) => v1 < v2,
            gt: (v1, v2) => v1 > v2,
            lte: (v1, v2) => v1 <= v2,
            gte: (v1, v2) => v1 >= v2,
            and() {
                return Array.prototype.every.call(arguments, Boolean);
            },
            or() {
                return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
            }
        });
        
    }

}


function runAllKatas(katasFromFile){
katasFromFile.forEach((kt, index) => {
        const id = uuid.v4();
        kt.id = id;
        let number = index + 1;
        const kataInstance = new kata(kt);
    try{
        kataInstance.kiai();
        const successmsg = 'executed correctly';

        if(kataInstance.name){
            execution.message += chalk.gray('\t'+ number + '. ' + 'Kata ' + id + '\n\t' + kataInstance.name + '\n\t\t'+chalk.green(successmsg)+'\n\n');
        }else{
            execution.message += chalk.gray('\t'+ number + '. ' + '\tKata\t'  + id + '\t\t' +chalk.green(successmsg)+ '\n');
        }
    }catch(err){
        execution.errors += 1;
        const errormsg = "with errors";
        if(kataInstance.name){
            execution.message += chalk.gray('\t'+ number + '. '+ 'Kata ' + id + '\n\t' + kataInstance.name + '\n\t\t'+chalk.red(errormsg)+'\n\n');
            setError(err, id);
        }else{
            execution.message += chalk.gray('\t'+ number + '. '+ 'Kata ' + id +  '\n\t\t'+chalk.red(errormsg)+'\n\n');
            setError(err, id);
        }

    }

    });

    if(execution.errors > 0){
        console.log(chalk.red(execution.header));
        console.log(chalk.gray(execution.message));
        console.log(chalk.red(execution.errorMessage));
    }else{
        console.log(chalk.gray(execution.header));
        console.log(execution.message);
    }
}
function setError(err, id){
            execution.errorMessage += "---------------------- kata " + id + " ----------------------\n\n";
            execution.errorMessage += err + "\n";
            execution.errorMessage += "\n---------------------------------------------------------------------------------------\n";
            execution.errorMessage += err.stack + "\n";
            execution.errorMessage += "\n---------------------------------------------------------------------------------------\n\n";
}

const header = "\n\n" +
  "\t               &&&&          &&.           \n "       
+ "\t       &%*&&@&&&&&&          &&&&&         \n "       
+ "\t       (&&&@&& &&           &&&&&          \n "       
+ "\t        &&      &&&       &&&&&            \n "       
+ "\t          &&&&&&&&@@&&@&/       &&@        \n "       
+ "\t     #&&&&&&&&&&&&&&  #         &&&&&      \n "       
+ "\t  &&&&&&&&&&&&  &&&&         &&&&&&&       \n "       
+ "\t     %    &@&&  &&&@      &&&&&&&/         \n "       
+ "\t         &&&&   /&&&             &&@       \n "       
+ "\t        &&&@@    &@&            &&&&&&*    \n "       
+ "\t      %&&&&      &&&          &&&&&&&      \n "       
+ "\t                 &&&        &&@&&&@        \n "       
+ "\t                 &&&     &&&&&%&           \n "       
+ "\t                  &&                       \n " 
+ "                                     "+chalk.gray("by:llucbrell  \n\n");



let execution = {header: header, message: "", errors: 0, errorMessage: ""};
const args = require('minimist')(process.argv.slice(2));
const katasFile = args.f;
const katasFromFile = JSON.parse(fs.readFileSync(katasFile, 'utf8'));
runAllKatas(katasFromFile);

