@Only-api @Post @Get @Delete @Patient
Feature: Patient resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Patient node. This resource is part of the Subject of Care.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Patient resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Patient'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Patient'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Patient resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Patient resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Patient/Patient.json')|



@Only-api @Post @Get @Delete @Condition
Feature: Primary Cancer Condtion resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Condition node. This resource is part of the Diagnosis.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Condition resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Condition'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Condition'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Condition resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Condition resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Condition/PrimaryCancer.json')|



@Only-api @Post @Get @Delete @Condition
Feature: Secondary Cancer Condtion resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Condition node. This resource is part of the Diagnosis.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Condition resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Condition'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Condition'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Condition resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Condition resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Condition/SecondaryCancer.json')|



@Only-api @Post @Get @Delete @Observation
Feature: Observation Diagnosis resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Observation node. This resource is part of the Diagnosis.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Observation resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Observation'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Observation'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Observation resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Observation resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Observation/ObservationDiagnosis.json')|



@Only-api @Post @Get @Delete @Encounter
Feature: Encounter resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Encounter node. This resource is part of the Diagnosis.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Encounter resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Encounter'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Encounter'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Encounter resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Encounter resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Encounter/Encounter.json')|



@Only-api @Post @Get @Delete @Organization
Feature: Organization resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Organization node. This resource is part of the Diagnosis.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Organization resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Organization'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Organization'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Organization resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Organization resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Organization/Organization.json')|



@Only-api @Post @Get @Delete @Observation
Feature: Hereditary Sindrome resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Observation node. This resource is part of the Diagnosis.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Observation resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Observation'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Observation'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Observation resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Observation resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Observation/HereditarySindrome.json')|



@Only-api @Post @Get @Delete @MedicationAdministration
Feature: Medication Administration resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific MedicationAdministration node. This resource is part of the Chemotherapy.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR MedicationAdministration resource.
    * def resource = <resource>
    * request <resource>
    Given path '/MedicationAdministration'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/MedicationAdministration'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the MedicationAdministration resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the MedicationAdministration resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/MedicationAdministration/MedicationAdministration.json')|



@Only-api @Post @Get @Delete @Observation
Feature: Cumulative Dose resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Observation node. This resource is part of the Chemotherapy.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Observation resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Observation'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Observation'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Observation resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Observation resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Observation/CumulativeDose.json')|



@Only-api @Post @Get @Delete @MedicationStatement
Feature: Intrathecal resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific MedicationStatement node. This resource is part of the Chemotherapy.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR MedicationStatement resource.
    * def resource = <resource>
    * request <resource>
    Given path '/MedicationStatement'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/MedicationStatement'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the MedicationStatement resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the MedicationStatement resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/MedicationStatement/Intrathecal.json')|



@Only-api @Post @Get @Delete @MedicationStatement
Feature: Corticosteroids resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific MedicationStatement node. This resource is part of the Chemotherapy.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR MedicationStatement resource.
    * def resource = <resource>
    * request <resource>
    Given path '/MedicationStatement'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/MedicationStatement'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the MedicationStatement resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the MedicationStatement resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/MedicationStatement/Corticosteroids.json')|



@Only-api @Post @Get @Delete @Location
Feature: Location resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Location node. This resource is part of the Chemotherapy,Major Surgery,Radiotherapy.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Location resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Location'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Location'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Location resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Location resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Location/Location.json')|



@Only-api @Post @Get @Delete @Procedure
Feature: Surgery resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Procedure node. This resource is part of the Major Surgery.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Procedure resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Procedure'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Procedure'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Procedure resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Procedure resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Procedure/Surgery.json')|



@Only-api @Post @Get @Delete @Procedure
Feature: Amputation resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Procedure node. This resource is part of the Major Surgery.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Procedure resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Procedure'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Procedure'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Procedure resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Procedure resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Procedure/Amputation.json')|



@Only-api @Post @Get @Delete @Procedure
Feature: Prosthesis resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Procedure node. This resource is part of the Major Surgery.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Procedure resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Procedure'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Procedure'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Procedure resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Procedure resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Procedure/Prosthesis.json')|



@Only-api @Post @Get @Delete @Procedure
Feature: Shunt resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Procedure node. This resource is part of the Major Surgery.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Procedure resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Procedure'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Procedure'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Procedure resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Procedure resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Procedure/Shunt.json')|



@Only-api @Post @Get @Delete @Procedure
Feature: Colostomy resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Procedure node. This resource is part of the Major Surgery.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Procedure resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Procedure'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Procedure'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Procedure resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Procedure resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Procedure/Colostomy.json')|



@Only-api @Post @Get @Delete @Procedure
Feature: Gastrostomy resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Procedure node. This resource is part of the Major Surgery.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Procedure resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Procedure'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Procedure'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Procedure resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Procedure resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Procedure/Gastrostomy.json')|



@Only-api @Post @Get @Delete @Procedure
Feature: Stem Cell Transplantation resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Procedure node. This resource is part of the Stem Cell Transplantation.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Procedure resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Procedure'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Procedure'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Procedure resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Procedure resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Procedure/StemCellTransplantation.json')|



@Only-api @Post @Get @Delete @BiologicallyDerivedProduct
Feature: Cell Donor Type and Source resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific BiologicallyDerivedProduct node. This resource is part of the Stem Cell Transplantation.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR BiologicallyDerivedProduct resource.
    * def resource = <resource>
    * request <resource>
    Given path '/BiologicallyDerivedProduct'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/BiologicallyDerivedProduct'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the BiologicallyDerivedProduct resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the BiologicallyDerivedProduct resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/BiologicallyDerivedProduct/CellDonor.json')|



@Only-api @Post @Get @Delete @Procedure
Feature: Graft Versus Host Desease Prophylaxis resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Procedure node. This resource is part of the Stem Cell Transplantation.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Procedure resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Procedure'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Procedure'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Procedure resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Procedure resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Procedure/GvhdProphylaxis.json')|



@Only-api @Post @Get @Delete @Condition
Feature: Graft Versus Host Desease resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Condition node. This resource is part of the Stem Cell Transplantation.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Condition resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Condition'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Condition'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Condition resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Condition resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Condition/Gvhd.json')|



@Only-api @Post @Get @Delete @Observation
Feature: Blood before and after tranplant resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific Observation node. This resource is part of the Stem Cell Transplantation.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR Observation resource.
    * def resource = <resource>
    * request <resource>
    Given path '/Observation'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/Observation'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the Observation resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the Observation resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/Observation/StcBlood.json')|



