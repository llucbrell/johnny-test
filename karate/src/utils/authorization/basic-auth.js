function fn(creds) {
 // karate.log('creds', creds);
  var temp = creds.apiuser + ':' + creds.apipass;
  var Base64 = Java.type('java.util.Base64');
  var encoded = Base64.getEncoder().encodeToString(temp.toString().getBytes());
  return 'Basic ' + encoded;
}