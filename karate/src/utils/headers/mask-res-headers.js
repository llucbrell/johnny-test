function fn(object) {

    // REQUEST
    var arrayOfHeadersToMask = object.mask;
    for (var i = 0; i < arrayOfHeadersToMask.length; i++) {
        object.headers[arrayOfHeadersToMask[i]] = '********';
    }
    karate.log(object.headers);
    return object.headers;
}
