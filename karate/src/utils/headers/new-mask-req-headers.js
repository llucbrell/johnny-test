function fn(object) {

    var headers = object.headers;
    // REQUEST
    var arrayOfHeadersToMask = object.mask;
    for (var i = 0; i < arrayOfHeadersToMask.length; i++) {
        headers[arrayOfHeadersToMask[i]] = '********';
    }
    return headers;
}
