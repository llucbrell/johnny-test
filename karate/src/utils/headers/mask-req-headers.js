function fn(arrayOfHeadersToMask) {
    var headers = karate.prevRequest.headers;
    for (var i = 0; i < arrayOfHeadersToMask.length; i++) {
        headers[arrayOfHeadersToMask[i]] = '********';
    }
    return headers;
}
