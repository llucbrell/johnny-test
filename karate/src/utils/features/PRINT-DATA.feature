Feature: Show the response
Shows the response headers masked for security reasons. The response body is not shown.

Scenario: Print the headers
    * REQUEST_HEADERS.Authorization = "********"; 
    * configure report = { showLog: true }
    * print REQUEST_HEADERS
    * print RESPONSE_HEADERS
    * print RESPONSE
    * print COOKIES
    * configure report = { showLog: false }
