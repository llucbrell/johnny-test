Feature: Sends a get with the needed headers
Imports headers and credentials from external files and set a get

    Scenario: Execute the GET request
    * configure report = false
    When method GET 
    * def headersToMask = ['Authorization']
    * configure report = true
    * def mivar = 'holaquetal'
    * def completeHeaders = call read('../../utils/headers/mask-headers.js') ['Authorization']
    * print completeHeaders