Feature: Delete the resources
Delete all the fhir resources from one node

Scenario: Delete the resources
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    * print resource.id
    * print "Deleting resource"
    Given  path "/" + resource.resourceType + "/" + resource.id
    * configure report = false
    When method DELETE
    * configure report = true
    Then status 200
    And print response