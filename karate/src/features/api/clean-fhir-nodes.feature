@Delete @Clean
Feature: Delete all resources from database
This feature deletes all the resources from the cineca platform fhir database.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 

    Scenario: Delete all the resources from Organization Resource
    Given path '/Organization'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from Location Resource
    Given path '/Location'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from Observation Resource
    Given path '/Observation'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from MedicationAdministration Resource
    Given path '/MedicationAdministration'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from MedicationStatement Resource
    Given path '/MedicationStatement'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from Procedure Resource
    Given path '/Procedure'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from Condition Resource
    Given path '/Condition'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from Encounter Resource
    Given path '/Encounter'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from Condition Resource
    Given path '/Condition'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from PlanDefinition Resource
    Given path '/PlanDefinition'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from DocumentReference Resource
    Given path '/DocumentReference'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from CarePlan Resource
    Given path '/CarePlan'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from Composition Resource
    Given path '/Composition'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


    Scenario: Delete all the resources from Patient Resource
    Given path '/Patient'
    * configure report = false
    When method GET 
    * configure report = true
    Then status 200
    And print response
    And def result = call read('../../utils/features/delete.feature') response.entry


   