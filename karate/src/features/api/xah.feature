@Only-api @Post @Get @Delete @MedicationAdministration 
Feature: Medication Administration resource actions
This feature checks the patient resource actions, get, post and delete made on FHIR specific MedicationAdministration node. This resource is part of the Chemotherapy.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
* configure report = { showAllSteps: false }

    Scenario Outline: The hospital sends a FHIR MedicationAdministration resource.
    * def resource = <resource>
    * request <resource>
    Given path '/MedicationAdministration'
    And print resource
    * configure report = { showLog: false }
    When method POST
    Then status 201
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}

    * def newPath = '/MedicationAdministration'+ '/' + response.id
    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They search on the server for the already created FHIR resource'
    And method GET
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    When print 'They try to delete the MedicationAdministration resource from the server'
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    And method DELETE
    Then status 200
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

    * path newPath
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When print 'They check if the MedicationAdministration resource is on the server'
    And method GET
    Then status 410
    * configure report = { showLog: true }
    And print response
    * configure report = { showLog: false }

Examples:
| read('../../mockdata/resources/MedicationAdministration/MedicationAdministration.json')|



