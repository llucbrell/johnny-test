@Only-api @Security @Credentials
Feature: Wrong Credentials for API
This feature checks that all resource nodes does not leave access to data without correct credentials.

Background:
* url baseUrl
* configure report = { showAllSteps: false }

    Scenario Outline: Correct response for bad credentials in Patient Resource
    Given path '/Patient'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in Condition Resource
    Given path '/Condition'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in Encounter Resource
    Given path '/Encounter'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in Organization Resource
    Given path '/Organization'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in Location Resource
    Given path '/Location'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in Observation Resource
    Given path '/Observation'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in DocumentReference Resource
    Given path '/DocumentReference'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in Composition Resource
    Given path '/Composition'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in MedicationAdministration Resource
    Given path '/MedicationAdministration'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in MedicationStatement Resource
    Given path '/MedicationStatement'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in PlanDefinition Resource
    Given path '/PlanDefinition'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in Procedure Resource
    Given path '/Procedure'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

    Scenario Outline: Correct response for bad credentials in CarePlan Resource
    Given path '/CarePlan'
    And def credentials = { apiuser: <user>, apipass: <password> }
    * def myHeaders = call read('../../utils/headers/basic-headers.js')
    * headers myHeaders
    * header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 
    When method get
    Then status 401
Examples:
|read('../../mockdata/fake_credentials.csv')|

