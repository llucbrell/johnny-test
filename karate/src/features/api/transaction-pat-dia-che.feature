@Post @Subject-of-care @Diagnosis @Chemotherapy @Transaction @Bundle 
Feature: Transaction demo-diag-chem
This feature sends a transaction with Subject of Care and Diagnosis and chemotherapy models

Background:
* configure report = { showAllSteps: false }
Given print baseUrl
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 

    Scenario Outline: The hospital sends a FHIR Patient transaction.
    * def resource = <transaction>
    * request <transaction>
    Given print resource
    * configure report = { showLog: false }
    When method POST
    Then status 200
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}
    And call read('../../utils/features/PRINT-DATA.feature') { REQUEST_HEADERS: '#(karate.prevRequest.headers)', RESPONSE_HEADERS: #(responseHeaders), COOKIES: #(responseCookies), RESPONSE: #(response)}


Examples:
| read('../../mockdata/transaction/DemoDiagChemTransactions.json')|



