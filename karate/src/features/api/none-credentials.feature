@Only-api @Security @Credentials 
Feature: None Credentials for API
This feature checks that all resource nodes does not leave access to data without credentials.

Background:
* url baseUrl
* header Accept = 'application/json'

    Scenario: Correct response for bad credentials in Patient Resource
    Given path '/Patient'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in Condition Resource
    Given path '/Condition'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in Encounter Resource
    Given path '/Encounter'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in Organization Resource
    Given path '/Organization'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in Location Resource
    Given path '/Location'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in Observation Resource
    Given path '/Observation'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in DocumentReference Resource
    Given path '/DocumentReference'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in Composition Resource
    Given path '/Composition'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in MedicationAdministration Resource
    Given path '/MedicationAdministration'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in MedicationStatement Resource
    Given path '/MedicationStatement'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in PlanDefinition Resource
    Given path '/PlanDefinition'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in Procedure Resource
    Given path '/Procedure'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

    Scenario: Correct response for bad credentials in CarePlan Resource
    Given path '/CarePlan'
    When method GET 
    Then status 401
    And print response
    And print responseHeaders
    And print responseCookies

