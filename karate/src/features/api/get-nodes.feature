@Only-api @Get 
Feature: Resource node 
This feature checks the nodes associated to each FHIR resource.

Background:
* url baseUrl
* def credentials = read('../../../../../credentials_and_other/credentials.json')
* def myHeaders = call read('../../utils/headers/basic-headers.js')
* headers myHeaders
* header Authorization = call read('../../utils/authorization/basic-auth.js') credentials 

    Scenario: Correct response for credentials in Patient Resource
    Given path '/Patient'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in Condition Resource
    Given path '/Condition'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in Encounter Resource
    Given path '/Encounter'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in Organization Resource
    Given path '/Organization'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in Location Resource
    Given path '/Location'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in Observation Resource
    Given path '/Observation'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in DocumentReference Resource
    Given path '/DocumentReference'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in Composition Resource
    Given path '/Composition'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in MedicationAdministration Resource
    Given path '/MedicationAdministration'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in MedicationStatement Resource
    Given path '/MedicationStatement'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in PlanDefinition Resource
    Given path '/PlanDefinition'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in Procedure Resource
    Given path '/Procedure'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies


    Scenario: Correct response for credentials in CarePlan Resource
    Given path '/CarePlan'
    * configure report = false
    When method GET 
    Then status 200
    * configure report = true
    And print response
    * def reqHeaders = call read('../../utils/headers/mask-req-headers.js') [ 'Authorization','Content-Type','Accept' ]
    * def resHeaders = call read('../../utils/headers/mask-res-headers.js') { headers: '#(responseHeaders)', mask:['Authorization'] }
    * print reqHeaders
    * print resHeaders
    * print responseCookies



    