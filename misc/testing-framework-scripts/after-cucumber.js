const fs = require('fs');
//define message type and reuse them whenever you want
const sysOut = require('./simple-logger');

sysOut.fileScript = __filename;
sysOut.header();

//const file = require('./misc/metadata/features-metadata.json')
let metadata = null;
let cucumberData = null;
try{
const rawMetaData = fs.readFileSync('./misc/metadata/features-metadata.json')
metadata = JSON.parse(rawMetaData)
}catch(e){
    sysOut.error('Error reading features-metadata.json ', e);
    sysOut.footer();
    process.exit(1);
}
try{
const cucumberRawData = fs.readFileSync('./jsonlogs/log.json')
cucumberData = JSON.parse(cucumberRawData)
}catch(e){
    sysOut.error('Error reading jsonlogs/log.json ', e);
    sysOut.footer();
    process.exit(1);
}

try{
// iterate over cucumber data using foreach
let warnings = 0;
let match = false;
cucumberData.forEach(function(feature) {
    // iterate over metadata using foreach
    match = false;
    metadata.forEach(function(metadata) {
    // check if feature has same name that metadata
    if (feature.name === metadata.name) {
        // if yes, merge the two objects
        Object.assign(feature, metadata);
        match = true;
        sysOut.sucess('Feature ' + feature.name + ' found in metadata');
    }
    })
    if (!match) {
        warnings += 1;
        sysOut.warn('Feature ' + feature.name + " not found in metadata");
    }

})
if(warnings == 0){
       sysOut.success('All features were found in metadata');
}


}catch(e){
    sysOut.error('Error treating the features metadata ', e);
    sysOut.footer();
    process.exit(1);
}

try{
    fs.writeFileSync('./jsonlogs/log.json', JSON.stringify(cucumberData, undefined, 4))
    sysOut.success('Features metadata merged without errors');
}catch(e){
    sysOut.error('Error trying to writ file to ./jsonlogs/log.json', e);
    sysOut.footer();
    process.exit(1);
}
sysOut.footer();