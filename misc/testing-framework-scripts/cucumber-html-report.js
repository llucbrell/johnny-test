const fs = require('fs');
const ncp = require('ncp').ncp;
const report = require("multiple-cucumber-html-reporter");
const path = require('path');
const sysOut = require('./simple-logger');
sysOut.fileScript = __filename;

const projectStratsAtTime = 'Sep 13, 2022';
const projectName = 'HULAFE - Cineca Platform BDTs';
const destinationReport = "./reports/cucumber-html-report";
const pageTitle = 'La Fe Tests';

const companyLogoClass = "icon-iislafe-logo";
const companyTooltip = "Instituto de Investigación Sanitaria La Fe";
const companyUrl = "https://www.iislafe.es/";
const companyFontsize = "35px" 
const logo2Class = "icon-lf-logo";
const logo2Tooltip = "Hospital Universitario y Politécnico La Fe";
const logo2Url = "http://www.hospital-lafe.com";
const logo2fontsize = "35px"
const logofooterClass = "icon-lf-logo-sqe";
const logofooterTooltip = "Go to La Fe website";
const logofooterUrl = "http://www.hospital-lafe.com/";
const logoGHubfooterUrl = "https://github.com/pcsp-hulafe";
const logoMailfooterUrl = "mailto:dev_pcsp_hulafe@gva.es";
const logoMailfooterTooltip = "Send an email to dev_pcsp_hulafe@gva.es";


const htmlTitle = '<p class="navbar-text"><a href="'+ companyUrl + '" title="'+ companyTooltip+'" target="_blank"><span class="'+companyLogoClass+'" style="font-size:'+companyFontsize+'"></span></a> <a href="'+logo2Url+'" target="_blank" title="'+logo2Tooltip+'"><span class="'+logo2Class+'" style="font-size:'+logo2fontsize+'"></span></a> </p>';
const htmlFooter = ' <div class="created-by"> <a href="'+logoGHubfooterUrl+'" target="_blank"><i class="fa fa-github-square fa-2x"></i><a>&nbsp;<a href="'+logofooterUrl+'" target="_blank"><span class="'+logofooterClass+'" title="'+logofooterTooltip+'"></span></a>&nbsp;<a href="'+logoMailfooterUrl+'" title="'+logoMailfooterTooltip+'" target="_blank"><i class="fa fa-envelope-square fa-2x"></i></a></div>';
// variables

// create a variable which contains the operating system name
const os = require('os');
const generateReport = require('multiple-cucumber-html-reporter');
const platform = os.platform();
const osVersion = os.release();
const today = new Date();



/*
const browser = Cypress.browser.name;
const browserVersion = Cypress.browser.version;
*/
const date = today.toLocaleDateString("en-US", {
  month: "short",
  day: "numeric",
  year: "numeric"
}).replace(/\s/g, " ");
const time = new Date().toLocaleTimeString("en-US", {
  hour12: false,
  hour: "numeric",
  minute: "numeric"
}).replace(/\s/g, " ");
// create a variable with time using meridian
const timeMeridian = new Date().toLocaleTimeString("en-US", {
  hour12: true,
  hour: "numeric",
  minute: "numeric"
}).replace(/\s/g, " ");


let sourceDir = path.join(__dirname, "../customassets");
let destDir = path.join(__dirname, "../../reports/cucumber-html-report/customassets");
  
// Represents the number of pending
// file system requests at a time.
ncp.limit = 16;
sysOut.header();

  if (!fs.existsSync(destDir)){
    try{
      fs.mkdirSync(destDir, { recursive: true });
    }catch(e){
      sysOut.error('Error trying to build the folder ' + destDir, e);
      sysOut.footer();
      process.exit(1);
    }
      copyCustomAssets();
  }else{
      generateHTMLCucumberReport();
  }
  
function copyCustomAssets() {

try{
ncp(sourceDir, destDir, 
        function (err) {
    if (err) {
        sysOut.warn(err);
    }else{
      sysOut.sucess('Folders copied recursively');
      generateHTMLCucumberReport();

    }
  

});
}catch(e){
    sysOut.error('Error copying data from ' + sourceDir + " to " + destDir, e);
    sysOut.footer();
    process.exit(1);
}


}

function generateHTMLCucumberReport() {
try{
report.generate({
  jsonDir: "jsonlogs", 
  reportPath: destinationReport, 
  metadata: {
    browser: {
      name: "unknown",
      version: "unknown",
    },
    device: "Local test machine",
    platform: {
      name: platform,
      version: osVersion,
    },
  },
  pageTitle: pageTitle,
  reportName: htmlTitle,
  pageFooter: htmlFooter,
  displayReportTime: true,
  displayDuration: false,
  durationInMs: false,
  customStyle: '../../../reports/cucumber-html-report/customassets/css/lf-for-test.css',
  customData: {
        title: 'General info',
        data: [
            {label: 'Project', value: projectName},
            {label: 'Release', value: '0.0.1'},
            {label: 'Methodology', value: 'Behavior Driven Testing'},
            {label: 'BDT - Starting date', value: projectStratsAtTime},
            {label: 'BDT - Last test', value: date},
            {label: '', value: ''},
            {label: 'Report opened at', value: timeMeridian},
        ]
    },    
}) 
sysOut.sucess('Cucumber HTML report generated');

}catch(e){
    sysOut.error('Error generating the repor', e);
    sysOut.footer();
    process.exit(1);
}
sysOut.footer();
}