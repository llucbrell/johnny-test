const chalk = require('chalk');


var asciis = function (){
  var self = this;
   self.version = function (author,  version){
        console.log(chalk.white.bold(this.horseTitle));
        console.log(chalk.blueBright(this.grass));
        console.log();
        console.log(chalk.gray("\tversion: " + version));
        console.log(chalk.gray("\tcompany: " + "hulafe \n"));
   }
   self.header = function (position, message){
    // create a random number between 1 and 3
    if(!message){
        message = "";
    }
    var randomNumber = Math.floor(Math.random() * 3) + 1;
    // print different things depending on the random number
    if (randomNumber == 1) {
        console.log(chalk.white.bold(this.horseRunning));
        console.log(chalk.gray("\t\t" + "free-reins-running: "+ message+ "\n"));
        console.log( position + chalk.gray(this.shortgrass + "\n"));
    } else if (randomNumber == 2) {
        console.log(chalk.white.bold(this.horseStopped));
        console.log(chalk.gray("\t\t" + "free-reins-running: "+ message+ "\n"));
        console.log( position + chalk.gray(this.shortgrass + "\n"));
    }
    else if (randomNumber == 3) {
        console.log(chalk.white.bold(this.horseJump));
        console.log(chalk.gray("\t\t" + "free-reins-running: "+ message+ "\n"));
        console.log( position + chalk.gray(this.shortgrass + "\n"));
    }
   }
   self.separator = function (){
        console.log(chalk.gray( "\n" + this.longrass + "\n"));
   }
    self.horseTitle = "" +
"       _ ____                   ___     _" + "\n" + 
"     /( ) _  \\                 | _ \\___(_)_ _  ___" + "\n" +
"    / //   /\\`\\, ||--||--||-   |   / -_) | ' \\(_-<" + "\n" +
"      \\|   |/ \\| ||--||--|free-|_|_\\___|_|_||_/__/" + "\n" +
"~^~^~^~~^~~~^~~^^~^^^^^^^^^^^^~^~^~^~~^~~~^~~^^~~^~^~^^~"; 

   self.horseRunning = "" +
"            .''" + "\n" +
"  ._.-.___.' (`\\" + "\n" +
" //(        ( `'" + "\n" +
"'/ )\\ ).__. ) " + "\n" +
"' <' `\\ ._/'\\"  + "\n" +
"   `   \\     \\   ";


   self.horseStopped = "" +
"       ,--," + "\n" +
"  _ ___/ /\|" + "\n" +
" ;( )__, ) " + "\n" +
"; //   '--; " + "\n" +
"  \\     |" + "\n" +
"   ^    ^        ";

   self.horseJump = "" +
"       \.*=." + "\n" +
"       /.\\|" + "\n" +
"    ./>' ( " + "\n" +
" _=/    ,^\\" + "\n" +
"~  \)-'   '" + "\n" +
"   / |  " + "\n" +
"  '  '";


   self.grass = "" +
"~^~^^~^~~^~~^~~~^~~^^~^~~^~~^~^~^~~^~~~^~~^^~~^~^~^^~" + "\n"+ 
"      ~^~^~^~~^~~~^~~^^~~^~^~^~~^~~~^~~^^~~^~^~^^~"; 
//    self.longrass = "" + 
//"~^~^~^~~^~~~^~~^^~~^~^~^~~^~~~^~~^^~~^~^~^^~~^~^~^~~^~~~^~~^^~~^~^~^~~^~~~^~~^^~~^~^~^^~"; 
//    self.shortgrass = "" + 
//"~^~^~^~~^~~~^~~^~^~^~~^~~~^~~^^~~^~^~^^~"; 

   self.longrass = "" +
//   "====================================================================================================";
   "────────────────────────────────────────────────────────────────────────────────────────────────────";
   self.shortgrass = "" +
    "";



}
module.exports = new asciis();




