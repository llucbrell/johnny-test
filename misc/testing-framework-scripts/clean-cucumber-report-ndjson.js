var fs = require('fs');
var sysOut = require('./simple-logger');
var logPath = './jsonlogs/messages.ndjson'; 


if (fs.existsSync(logPath)){

try{
    fs.unlinkSync(logPath);
    console.log();
    sysOut.sucess('Deleted ' + logPath + ' file');
    console.log();
}catch(e){
        console.log();
        sysOut.error('Error deleting ' + logPath, err);
        console.log();
        process.exit(1);
}

}else{
        console.log();
        sysOut.warn('There is no ' + logPath + ' file');
        console.log();
}
