const uuid = require('uuid')
const fs = require('fs');
const sysOut = require('./simple-logger');
sysOut.fileScript = __filename;
    
sysOut.header();
    const generatedUuid =  uuid.v4();
    const logname =  "cy_" + (new Date().toJSON().slice(0,10))+ '_' + generatedUuid.slice(0,16)+ ".json";
    const jsologPath = './jsonlogs/log.json';
    const metaPath ='./misc/metadata/features-metadata.json';

if (fs.existsSync(jsologPath)){
try{
    fs.rename(jsologPath, './jsonlogs/'+ logname, function(err) {
        if(err) {
        sysOut.error('Error renaming log.json', err);
        sysOut.footer();
        process.exit(1);
        }
        sysOut.sucess('Cleaned up datalogs');
        sysOut.sucess('Log file renamed to ' + logname);
    });
}catch(e){
        sysOut.error('Error renaming log.json', err);
        sysOut.footer();
        process.exit(1);
}
}else{
    sysOut.warn('No log.json found');
    sysOut.footer();
}
if (fs.existsSync(metaPath)){
try{
    fs.writeFile( metaPath, '[]', function(err) {
        if(err) {
            sysOut.error('Error writting features-metadada.json', err);
            sysOut.footer();
            process.exit(1);
        }
        sysOut.sucess('Cleaned up features-metadata.json');
        sysOut.footer();
      });
}catch(e){
        sysOut.error('Error writting features-metadada.json', err);
        sysOut.footer();
        process.exit(1);
}
}else{
    sysOut.warn('No features-metadata.json found');
    sysOut.footer();
}