const fs = require('fs');
const uuid = require('uuid');
//define message type and reuse them whenever you want
const sysOut = require('./simple-logger');

sysOut.fileScript = __filename;
sysOut.header();

const karatedir = './reports/karate-html-report/karate-reports/';
const arch = process.arch;
const os = process.platform;
let karateFeaturesToSave = [];

try{
const files = fs.readdirSync(karatedir);
files.forEach(function(file) {
    if (file.endsWith('.json')) {
        const data = fs.readFileSync( karatedir + file, 'utf8');
        sysOut.sucess( "Found feature" + file);
        addMetadataToFeature(data, file);
    }
});
saveFeaturesToFile();
sysOut.footer();
}catch(e){
    sysOut.error('Error reading the karate directory ' + karatedir, e);
    sysOut.footer();
    process.exit(1);
}

function addMetadataToFeature(data, fileName){
    const fileNameParts = fileName.split('.');
    let karateTestType = fileNameParts[fileNameParts.length - 3];
    karateTestType = karateTestType.toUpperCase();
    let features = JSON.parse(data);
    let feature = features[0]; 
    if (karateTestType === 'API'){
        setFeatureApiMetadata(feature, karateTestType);
    }
    

}

function setFeatureApiMetadata(feature, karateTestType){
    feature.metadata = {
            browser:{
                name: "None",
                version: karateTestType + " testing"
            },
            device: "Karate "+ karateTestType.toLowerCase(), 
            platform: {
                name: os,
                version: arch,
                },
    };
    feature.testingFramework = {
                name: "Karate",
                version: "1.2.1.RC1",
                testingType: karateTestType + " Testing",
    };
    const descriptionraw = feature.description;
    const descriptionArra = descriptionraw.split('\n');
    feature.name = descriptionArra[0];
    feature.description = descriptionArra.slice(1).join('\n');
    karateFeaturesToSave.push(feature);
}


function saveFeaturesToFile(){
    const generatedUuid =  uuid.v4();
    const logname =  "krt_" + (new Date().toJSON().slice(0,10))+ '_' + generatedUuid.slice(0,16)+ ".json";
    const logsDir = './jsonlogs/';
try{
    fs.writeFileSync( logsDir + logname, JSON.stringify(karateFeaturesToSave, undefined, 4));
    sysOut.success('Features metadata merged and saved without errors in file ' + logname);
}catch(e){
    sysOut.error('Error trying to writ file to ' + logsDir + ' ' + logname, e);
    sysOut.footer();
    process.exit(1);
}
}