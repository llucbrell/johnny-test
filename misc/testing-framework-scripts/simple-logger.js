const chalk = require('chalk');
const asciis = require('./ascii-art/inline-art');

var sysOut = function (){
   this.messagePositions = "\t\t"
   this.success = function (message){
      console.log( this.messagePositions+ chalk.green( "♯ "+ message));
   }
   this.sucess = function (message){
      console.log( this.messagePositions+ chalk.green.bold( "♯ " )+ chalk.gray(message));
   }
   this.error = function (message, error){
      console.log( this.messagePositions+  chalk.red( "♭ " + message + "\n\n" + error));
   }
   this.warn = function (message){
      console.log( this.messagePositions+ chalk.yellow.bold( "♮ " )+ chalk.gray(message));
   }
   this.title = function (author, version){
      asciis.separator();
      asciis.version(author, version);
      //asciis.separator();
    }
   this.header = function (){
      asciis.separator();
      //asciis.version();
      asciis.header(this.messagePositions, this.fileScript);
   }
   this.footer = function (){
      asciis.separator();
   }
   this.fileScript = "";

};

module.exports = new sysOut();