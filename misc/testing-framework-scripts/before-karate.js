const path = require('path');
const fs = require('fs');
const sysOut = require('./simple-logger');

let destDir = path.join(__dirname, "../../reports/karate-html-report");
let jsonDir = path.join(__dirname, "../../jsonlogs");


 if (!fs.existsSync(destDir)){
    try{
      fs.mkdirSync(destDir, { recursive: true });
    }catch(e){
      sysOut.error('Error trying to build the folder ' + destDir, e);
      sysOut.footer();
      process.exit(1);
    }
}
 if (!fs.existsSync(jsonDir)){
    try{
      fs.mkdirSync(jsonDir, { recursive: true });
    }catch(e){
      sysOut.error('Error trying to build the folder ' + jsonDir, e);
      sysOut.footer();
      process.exit(1);
    }

}
