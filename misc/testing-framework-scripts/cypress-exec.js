var sysOut = require('./simple-logger');
sysOut.fileScript = __filename;
sysOut.header();


const exec = require("child_process").exec;
  return new Promise((resolve, reject) => {
    exec(cmd, { maxBuffer: 1024 * 500 }, (error, stdout, stderr) => {
      if (error) {
        sysOut.warn(error);
      } else if (stdout) {
        console.log(stdout); 
      } else {
        sysOut.warn(stderr);
      }
      resolve(stdout ? true : false);
      reject(sysOut("Error executing command " + cmd));
      sysOut.footer();
    });

  });
