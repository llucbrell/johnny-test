
<img src="./readme-screenshots/logo_hulafe_test.png" style="width: 150px" alt="ejemplo imagen">

# Hulafe Johnny Test Framework

## Table of Contents
1. [Pre-requisites](#pre-requisites)
2. [What is it?](#what-is-it)
3. [Installation](#installation)
4. [How to work with ](#how-to-work-with)
5. [Next steps](#next-steps)
6. [Topics](#topics)
7. [Thanks](#thanks)



## Pre-requisites

Before you use this project you only need to have Git and Node-Js installed in your computer.

https://nodejs.org/es/download/

https://git-scm.com/

To get the best profit from this repo it would be good to have a basic knowledge of programming and also to know a little of javascript.

### What is it?

This is a boilerplate project that helps in the automatization of Behavior Driven Testing (BDD) using cypress with cucumber for frontend tests and karate for api tests. It helps to initialize any web project testing framework easily. It was used for testing different parts of the software usde for the SurPass project.

#### It lincludes

> Cypress 10+ with Cucumber boilerplate project.
> Karate dsl included in the repo

#### What you see is what you get

Some screenshots to see the project in action: 

<img src="./readme-screenshots/lf-screenshot-23-08-22.PNG" alt="ejemplo imagen">

Clicking on the name of the feaature inside report table you can also access to the feature test data.

<img src="./readme-screenshots/lf-screenshot-23-08-22-feature.PNG" alt="ejemplo imagen">

The tests are defined using gherkin language. This language is better for communication between developers and non-technical clients.

<img src="./readme-screenshots/lf-screenshot-23-08-22-features-open.PNG" alt="ejemplo imagen">

Each step is marked with som visual help (green and red) for passed or not passed test step.
You can find also embeded screenshots and error screenshots with log data to help developers to debug.

<img src="./readme-screenshots/lf-screenshot-23-08-22-features-open-screenshots.PNG" alt="ejemplo imagen">

<img src="./readme-screenshots/lf-screenshot-23-08-22-features-open-error.PNG" alt="ejemplo imagen">

On the part of the api testing, the features not include screenshots but you can find request, reponse data and also cookies and other features.

<img src="./readme-screenshots/sp-rest-req-resp-23-08-22.PNG" alt="ejemplo imagen">

This boilerplate for testing could be changed and  modified for other purposes, next you will find some commands and data to use it correctly.

## Installation

To install this BDT automation framework you should:

In the terminal go to the folder where you want to stablish the testing station.
Download this repo or clone it with
    
```bash
git clone CLONE-URL
``` 

Get inside the new created folder and then Install the project dependencies with:
```bash
npm install
```

### Target platform

To point to a base url in Karate you must set in **karate-config.js** file


```javaScript
baseUrl: 'https://pcsp-my-url-to-pre/fhir/'
```

and for cypress the same but in **cypres.config.js** file


### Behind a proxy

For cypress you can set an environment variable or consult the documentation.
Usually it works without any configuration but for karate you have to go to karate-config.js file and add

```javaScript
karate.configure('proxy',  { uri: 'http://ip.of.my.proxy:port'});
```

### The credentials

In the SurPass project, Karate and cypress were used with credentials to log to the platform and to mask them in the reports and also to have credentials outside code. So we inserted some features to make it possible without dealing with lots of troubles. 
If you decided to use it with jenkins, you have to use a file called credentials.json in a folder called credentials_and_other and put that folder in the parent directory of the testing framework.

```json
{
    "username": "my_user_for_cypress",
    "password": "my_password_for_login",
    "apiuser": "my_fhir_user",
    "apipass": "my_passwored"
}
```

## How to work with 

#### Cypress

Go to **cypress** folder and add cucumber tests inside **e2e** folder.

* Gherking inside **features** folder.
* Cucumber code inside **step_definitions** folder.
* Use **pages** folder if you want to use the Object page model pattern.

#### Run the tests

##### Cypress (browser-component-e2e tests)

Open the terminal and run:

```bash
npm run cuc:reports:html
```
This executes all tests and generates a report in **reports** folder. Executes all the tests built using cypress, that you can find all the tests inside the folder /cypres/e2e/

You can also set the target browser with:

```bash
npm run cuc:reports:html chrome
```


You can also run only particular tagged tests @My-tag using

```bash
npx cypress run -e TAGS="@Only-browser" -b edge
```
This command executes only the scenarios tagged as @Only-browser in the edge browser.
As you can see you can run any cypress command  or use the predefined one you can find in the package.json file inside scripts section.


> Be careful, if you run cypress without npm, it would not generate the report data correctly. All metadata will be lost. So we suggest to allways call predefined commads through npm.

At the Jenkins file you could see some workflow that were used for different tests. This might be usefull to compare and look at the way this framework works.

##### Karate (api tests)

Open the terminal and run:

```bash
npm run krt:reports:cuc
```
This command build a new cucumber-json file to integrate with the multiple-cucumber-html-reporter.


```bash
npm run krt:reports:html
```

This command creates the classic karate-html-report inside the **reports** folder.

```bash
npm run krt:reports:cuc:tags @Transaction
```
This last command only executes the scenarios tagged as @Transaction

##### Kata

Inside Karate folder you will find a test format generator called Kata. This script can generate formatted data form a template and a csv file.

You can find the templates inside **tatamis** folder, data inside **katas** folder. The script take the data from each column of csv and put them in the specific part of the tatami-template. How the data is combined comes from configuration files like kata-generate-miscellaneous-features.json and so on. This files mark inputs and outputs an if you will you can change them and use other data and tatamis.

Some aspects to have in account are.. kata replace data from inside two curlybraces (uses handlebars as engine) and you can use the same vocabulary.

###### Example of Kata
An example of using a kata template is 

```json
{ 
    "myJson":{
        {{kata}}"repeat": "{{myvariable}}",{{/kata}}
    }
```
If we have a csv file with three rows and values for myvariable column a,b,c we will get after running 

```bash
node ./kata.js
```
The output 
```json
{ 
    "myJson":{
        "repeat": "a",
        "repeat": "b",
        "repeat": "c",
    }
```
You can explore and play with the framework..

### More commands

```bash
npm run cy:exec
```

This command launch the cypress´s tests and generates the json-data inside **jsonlogs** folder, also creates a json file with metadata inside **misc/metadata**.

After that you can manipulate or add automatic tests into **jsonlogs** and when you decide, you can generate the report using:

```bash
npm run cuc:reports:html
```



If you find that you can run the karate tests use this command to regenerate folder

```bash
npm run tatami:before
```


> **Note: All the commands interns are set inside folder script and scripts-win.** Navigate to figure what they do.

#### Put the html report wherever you want to

The report script prints the destionation folder into the console. By default it is in the **reports/cucumber-html-report** folder.

We suggest to not to change the report configurations if you are not sure about what you are doing it, the framework could be break.



## Next steps

We are also dealing with more automation test systems. So we are consider to add new javascrit tools like..

* Selenium Integration
* Appium Integration
* Nightwatch/WebdriverIO Integration 


Nevertheless any feature would be rendered in the report if it has the correct json-data structure (examples in the jsonlogs). It must include metadata as in those example files (if not system will render a default metadata), so you can work with the programming language and automation system you desire.

## Topics
Integrated with:

* https://github.com/badeball/cypress-cucumber-preprocessor
* https://github.com/bahmutov/cypress-esbuild-preprocessor
* https://www.npmjs.com/package/multiple-cucumber-html-reporter
* https://github.com/karatelabs/karate
* https://github.com/karatelabs/karate/wiki/ZIP-Release

Some examples: 
* https://github.com/badeball/cypress-cucumber-preprocessor/tree/master/examples

Some docs:
* https://karatelabs.github.io/karate/karate-netty/
* https://cucumber.io/docs/gherkin/languages/
* https://github.com/karatelabs/karate/blob/master/karate-netty/README.md#standalone-jar
* https://www.chaijs.com/api/bdd/#method_match

To implement:
* https://github.com/karatelabs/karate/wiki/Karate-Robot-Windows-Install-Guide
* https://appium.io/


## Thanks

Thanks to the community of Cypress Cucumber, Karate and also to this project(  [Cypress Cucumber Boilerplate](https://github.com/JoanEsquivel/cypress-cucumber-boilerplate) ) who inspire us to build this repo using this methodology.